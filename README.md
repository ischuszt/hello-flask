## Example WSGI Flask app

In order to run this locally, run:

```bash
export FLASK_APP="src/hello_flask/app.py" && flask run
```

### Deploying via gunicorn

Run `pip install -e . `, then:

```bash
gunicorn -b 0.0.0.0:8000 hello_flask.wsgi:app
```