#!/usr/bin/env python

"""
setup.py
"""

from setuptools import setup, find_packages

requires = []
with open("requirements.txt") as f:
    requires = list(map(lambda line: line.strip(), f.readlines()))

setup(
    name="hello-flask-cristi",
    version="0.0.1",
    description="",
    author="Cristian Schuszter",
    author_email="cristian.schuszter@cern.ch",
    license="MIT",
    url="",
    packages=find_packages("src/"),
    package_dir={"": "src"},
    classifiers=[
        "Development Status :: 4 - Beta",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Programming Language :: Python :: 3.4",
    ],
    install_requires=requires,
)
